<?php
namespace Packages\Media\Custom\Repositories\Eloquent;

use Packages\Media\Repositories\Eloquent\EloquentMediaRepositories as CoreEloquentMediaRepositories;

class EloquentMediaRepositories extends CoreEloquentMediaRepositories implements \Packages\Media\Custom\Repositories\MediaRepositories
{
}