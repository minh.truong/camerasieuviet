import ImportImage from '@theme/support/import-image';
class BannerImage extends ImportImage{
    constructor() {
        super();
    }
}

export default BannerImage;