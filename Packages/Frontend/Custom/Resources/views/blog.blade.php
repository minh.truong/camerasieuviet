@extends('frontend.custom::layouts.detail')
@section('detail')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="carousel-heading">
                <h4>{{ $post->title }}</h4>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="blog-item">
                <div class="blog-info">
                    <div class="blog-meta">
                        <span class="date"><i class="icons icon-clock"></i> 21 December 2012</span>
                        <span class="cat"><i class="icons icon-tag"></i> <a href="#">lorem</a>, <a href="#">tablet</a></span>
                        <span class="views"><i class="icons icon-eye-1"></i> 11 times</span>
                        <div class="rating-box">
                            <span>Rate this item</span>
                            <div class="rating readonly-rating" data-score="4"></div>
                            <span>(1 vote)</span>
                        </div>
                    </div>
                    <div class="img-fluid img-thumbnail" style="margin: 20px auto 20px auto">
                        @if(isset($post->thumbImg()->path_org))
                            <img src="{{ asset('storage/'. $post->thumbImg()->path_org) }}" alt="{{ $post->title }}">
                        @endif
                    </div>
                    {!! $post->content !!}

                    {{--<div class="social-share">--}}
                        {{--<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px; width:100px;" allowTransparency="true"></iframe>--}}

                        {{--<iframe allowtransparency="true" frameborder="0" scrolling="no"--}}
                                {{--src="https://platform.twitter.com/widgets/tweet_button.html"--}}
                                {{--style="width:100px; height:20px;"></iframe>--}}

                        {{--<!-- Place this tag where you want the +1 button to render. -->--}}
                        {{--<div class="g-plusone" data-size="medium"></div>--}}

                        {{--<!-- Place this tag after the last +1 button tag. -->--}}
                        {{--<script type="text/javascript">--}}
                            {{--(function() {--}}
                                {{--var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;--}}
                                {{--po.src = 'https://apis.google.com/js/platform.js';--}}
                                {{--var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);--}}
                            {{--})();--}}
                        {{--</script>--}}


                        {{--<a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F&media=http%3A%2F%2Ftest.ratkosolar.com%2Fhomeshop%2F15-blog_post.html&description=Next%20stop%3A%20Pinterest" data-pin-do="buttonPin" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>--}}
                        {{--<!-- Please call pinit.js only once per page -->--}}
                        {{--<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>--}}

                    {{--</div>--}}

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @component('frontend.custom::components.widgets.latest-news')
        @endcomponent
    </div>

@endsection
