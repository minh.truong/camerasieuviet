<!-- News -->
<div class="products-row col-md-12">

    <!-- Carousel Heading -->
    <div class="col-lg-12 col-md-12 col-sm-12">

        <div class="carousel-heading">
            <h4>Latest news &amp; Reviews</h4>
            <div class="carousel-arrows">
                <i class="icons icon-left-dir"></i>
                <i class="icons icon-right-dir"></i>
            </div>
        </div>

    </div>
    <!-- /Carousel Heading -->


    <!-- Carousel -->
    <div class="carousel owl-carousel-wrap col-lg-12 col-md-12 col-sm-12">

        <div class="owl-carousel" data-max-items="2">
            @foreach($news as $n)
            <!-- Slide -->
            <div>
                <!-- Carousel Item -->
                <article class="news">
                    <div class="news-background">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 news-thumbnail">
                                <a href="{{ $n->slug }}"><img src="{{ asset('storage/'. $n->thumbImg()->path_medium) }}" alt="{{ $n->title }}"></a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 news-content">
                                <h5><a href="{{ route('frontend.post.detail', $n->slug) }}">{{ $n->title }}</a></h5>
                                <span class="date"><i class="icons icon-clock-1"></i> {{ UtilFacade::formatDateTime($n->created_at) }}</span>
                                <p>{{ $n->desc }}.</p>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- /Carousel Item -->
            </div>
            <!-- /Slide -->
            @endforeach

        </div>

    </div>
    <!-- /Carousel -->

</div>
<!-- /News -->