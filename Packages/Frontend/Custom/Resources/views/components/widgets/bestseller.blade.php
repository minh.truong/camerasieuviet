<!-- Bestsellers -->
<div class="row sidebar-box red">
    @component('frontend.custom::components.mini-slider')
    @slot('products', $products)
    @slot('title', 'Best seller')
    @endcomponent
</div>
<!-- /Bestsellers -->