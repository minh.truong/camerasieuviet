<!-- Banner -->
<section class="banner col-md-12">

    <div class="left-side-banner banner-item icon-on-right gray">
        <h4>{{ $config[\Packages\General\Config\GeneralConfig::GENERAL_SYSTEM_CONFIG_PHONE] }}</h4>
        <p>{{ $config[\Packages\General\Config\GeneralConfig::GENERAL_SYSTEM_CONFIG_WORKING_TIME] }}</p>
        <i class="icons icon-phone-outline"></i>
    </div>

    <a href="#">
        <div class="middle-banner banner-item icon-on-left light-blue">
            <h4>Free shipping</h4>
            <p>on all orders over $99</p>
            <span class="button">Learn more</span>
            <i class="icons icon-truck-1"></i>
        </div>
    </a>

    <a href="#">
        <div class="right-side-banner banner-item orange">
            <h4>Crazy sale!</h4>
            <p>on selected items</p>
            <span class="button">Shop now</span>
        </div>
    </a>

</section>
<!-- /Banner -->