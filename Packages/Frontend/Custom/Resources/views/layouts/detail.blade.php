@extends('frontend.custom::layouts.frontend')
@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="breadcrumbs">
            <p><a href="#">Home</a> <i class="icons icon-right-dir"></i> <a href="#">Computers &amp; Tablets</a> <i class="icons icon-right-dir"></i> Product Name</p>
        </div>
    </div>
    <!-- Main Content -->
    <section class="main-content col-lg-9 col-md-9 col-sm-9">
        @yield('detail')
    </section>
    <aside class="sidebar right-sidebar col-lg-3 col-md-3 col-sm-3">
        @component('frontend.custom::components.widgets.categories')
        @endcomponent
        @component('frontend.custom::components.widgets.random-products-slider')
        @endcomponent
        @component('frontend.custom::components.widgets.bestseller')
        @endcomponent

        <!-- Tag Cloud -->
        <div class="row sidebar-box green">

            <div class="col-lg-12 col-md-12 col-sm-12">

                <div class="sidebar-box-heading">
                    <i class="icons icon-tag-6"></i>
                    <h4>Tags Cloud</h4>
                </div>

                <div class="sidebar-box-content sidebar-padding-box">
                    <a href="#" class="tag-item">digital camera</a>
                    <a href="#" class="tag-item">lorem</a>
                    <a href="#" class="tag-item">gps</a>
                    <a href="#" class="tag-item">headphones</a>
                    <a href="#" class="tag-item">ipsum</a>
                    <a href="#" class="tag-item">laptop</a>
                    <a href="#" class="tag-item">smartphone</a>
                    <a href="#" class="tag-item">tv</a>
                </div>

            </div>

        </div>
        <!-- /Tag Cloud -->
    </aside>
    <!-- /Sidebar -->

    @component('frontend.custom::components.widgets.banner')
    @endcomponent
@endsection

@section('scripts')
@endsection
