<?php
namespace Packages\Frontend\Custom\ViewComposers;
use Illuminate\View\View;
use Packages\General\Custom\Services\GeneralServices;
use Packages\Post\Custom\Services\PostServices;
use Packages\Product\Custom\Services\CategoryServices;
use Packages\Product\Custom\Services\ProductServices;

class FrontendLatestNewsWidgetComposer {

    public function __construct()
    {
    }

    public function compose(View $view)
    {
        $view->with('news', app()->make(PostServices::class)->filter(['status' => 'P'])->orderByDesc('created_at')->get());
    }
}