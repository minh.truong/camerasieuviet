<?php
namespace Packages\Frontend\Custom\Repositories\Eloquent;

use Packages\Frontend\Repositories\Eloquent\EloquentFrontendRepositories as CoreEloquentFrontendRepositories;

class EloquentFrontendRepositories extends CoreEloquentFrontendRepositories implements \Packages\Frontend\Custom\Repositories\FrontendRepositories
{
}