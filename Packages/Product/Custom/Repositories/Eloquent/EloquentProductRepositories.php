<?php
namespace Packages\Product\Custom\Repositories\Eloquent;

use Packages\Product\Repositories\Eloquent\EloquentProductRepositories as CoreEloquentProductRepositories;

class EloquentProductRepositories extends CoreEloquentProductRepositories implements \Packages\Product\Custom\Repositories\ProductRepositories
{
}