<?php
namespace Packages\System\Custom\Repositories\Eloquent;

use Packages\System\Repositories\Eloquent\EloquentSystemRepositories as CoreEloquentSystemRepositories;

class EloquentSystemRepositories extends CoreEloquentSystemRepositories implements \Packages\System\Custom\Repositories\SystemRepositories
{
}