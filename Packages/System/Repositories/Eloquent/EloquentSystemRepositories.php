<?php
namespace Packages\System\Repositories\Eloquent;


use Packages\Core\Traits\Repositories\PackageRepositoriesTrait;
use Packages\System\Repositories\SystemRepositories;

class EloquentSystemRepositories implements SystemRepositories
{
    use PackageRepositoriesTrait;
}