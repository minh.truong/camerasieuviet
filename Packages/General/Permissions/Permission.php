<?php
namespace Packages\General\Permissions;

interface Permission
{
    // Access GENERAL
    const GENERAL_ACCESS = 'GENERAL_ACCESS';
    const GENERAL_EDIT = 'GENERAL_EDIT';
    const GENERAL_DELETE = 'GENERAL_DELETE';
}