<?php
/**
 * The interface of corerole services
 */
namespace Packages\Core\Services;

interface UtilServices {
    /**
     * Generate a slug from string
     * @param String $string
     * @return String slug
     */
    public function generateSlug($string);

    /**
     * Auto convert datetime to string match with the system config
     * @param \DateTime $dateTime
     * @param string $type: d: Date, t: time, dt: datetime
     * @return string
     */
    public function formatDateTime($dateTime, $type = 'd');

    /**
     * Format number
     * @param double $number
     * @param int $delimiter
     * @return double
     */
    public function formatNumber($number, $delimiter = null);
}