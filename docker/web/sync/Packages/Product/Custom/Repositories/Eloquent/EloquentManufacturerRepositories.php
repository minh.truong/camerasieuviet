<?php
namespace Packages\Product\Custom\Repositories\Eloquent;

use Packages\Product\Repositories\Eloquent\EloquentManufacturerRepositories as CoreEloquentManufacturerRepositories;

class EloquentManufacturerRepositories extends CoreEloquentManufacturerRepositories implements \Packages\Product\Custom\Repositories\ManufacturerRepositories
{
}