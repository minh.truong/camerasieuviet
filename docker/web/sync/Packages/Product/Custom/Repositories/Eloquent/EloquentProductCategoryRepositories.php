<?php
namespace Packages\Product\Custom\Repositories\Eloquent;
use Packages\Product\Repositories\Eloquent\EloquentProductCategoryRepositories as CoreRepository;

class EloquentProductCategoryRepositories extends CoreRepository implements \Packages\Product\Custom\Repositories\ProductCategoryRepositories
{
}