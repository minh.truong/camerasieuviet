<?php
namespace Packages\Frontend\Repositories\Eloquent;


use Packages\Core\Traits\Repositories\PackageRepositoriesTrait;
use Packages\Frontend\Repositories\FrontendRepositories;

class EloquentFrontendRepositories implements FrontendRepositories
{
    use PackageRepositoriesTrait;
}