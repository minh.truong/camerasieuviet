<?php
return [
    'package'  => 'Frontend',
    'use-multiple-images'  => 'Use Multiple Images',
    'banner'  => 'Banner',
    'confirm-delete-msg'    => 'Are you sure to delete %s ?',
    'confirm-delete-warn-msg'    => 'You will not be able to revert this!',
    'yes-confirm-delete-btn'    => 'Yes, delete it!',
];