<?php
namespace Packages\Frontend\Custom\Repositories\Eloquent;

use Packages\Frontend\Repositories\Eloquent\EloquentBannerRepositories as CoreEloquentBannerRepositories;

class EloquentBannerRepositories extends CoreEloquentBannerRepositories implements \Packages\Frontend\Custom\Repositories\BannerRepositories
{
}