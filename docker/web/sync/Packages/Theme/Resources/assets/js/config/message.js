export default {
    UPDATE_SUCCESS: 'The %s has been updated successfully.',
    CREATED_SUCCESS: 'New %s has been created successfully.',
    DELETED_SUCCESS: '%s has been deleted successfully.',
}