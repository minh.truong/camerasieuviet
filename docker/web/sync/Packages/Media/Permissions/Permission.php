<?php
namespace Packages\Media\Permissions;

interface Permission
{
    // Access MEDIA
    const MEDIA_ACCESS = 'MEDIA_ACCESS';
    const MEDIA_UPLOAD = 'MEDIA_UPLOAD';
    const MEDIA_DELETE = 'MEDIA_DELETE';
}