<?php
return [
    'package'  => 'Media',
    'upload-desc'  => 'Upload your media files',
    'confirm-delete-msg'    => 'Are you sure to delete this file?',
    'confirm-delete-warn-msg'    => 'You will not be able to revert this!',
    'yes-confirm-delete-btn'    => 'Yes, delete it!',
];