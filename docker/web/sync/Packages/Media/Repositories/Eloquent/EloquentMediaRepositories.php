<?php
namespace Packages\Media\Repositories\Eloquent;


use Packages\Core\Traits\Repositories\PackageRepositoriesTrait;
use Packages\Media\Repositories\MediaRepositories;

class EloquentMediaRepositories implements MediaRepositories
{
    use PackageRepositoriesTrait;
}