<?php
namespace Packages\General\Repositories\Eloquent;


use Packages\Core\Traits\Repositories\PackageRepositoriesTrait;
use Packages\General\Repositories\GeneralRepositories;

class EloquentGeneralRepositories implements GeneralRepositories
{
    use PackageRepositoriesTrait;
}