<?php
namespace Packages\General\Custom\Repositories\Eloquent;

use Packages\General\Repositories\Eloquent\EloquentGeneralRepositories as CoreEloquentGeneralRepositories;

class EloquentGeneralRepositories extends CoreEloquentGeneralRepositories implements \Packages\General\Custom\Repositories\GeneralRepositories
{
}