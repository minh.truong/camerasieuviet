<?php
namespace Packages\General\Entities;
use Illuminate\Database\Eloquent\Model;

class General extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
}