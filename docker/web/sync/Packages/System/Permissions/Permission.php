<?php
namespace Packages\System\Permissions;

interface Permission
{
    // Access SYSTEM
    const SYSTEM_ACCESS = 'SYSTEM_ACCESS';
    const SYSTEM_EDIT = 'SYSTEM_EDIT';
    const SYSTEM_DELETE = 'SYSTEM_DELETE';
}