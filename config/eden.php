<?php
return [

    /*
     * Default prefix for backend routing
    */

    'backend_prefix_route'  => 'console',
    'ajax_prefix_route'  => 'ajax',
];